<?php
/**
 * @file
 * sif_queues.module
 */

/**
 * Implements hook_menu().
 */
function sif_queues_menu() {
  $items = array();
  $items['admin/sif/queues'] = array(
    'title' => 'Queues',
    'description' => 'Create and manage SIF queues.',
    'page callback' => 'sif_queues_page',
    'access arguments' => array('configure sif'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'pages.inc',
  );
  $items['admin/sif/queues/create'] = array(
    'title' => 'Create Queue',
    'description' => 'Create a new queue.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sif_queues_create'),
    'access arguments' => array('configure sif'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'pages.inc',
  );
  $items['admin/sif/queues/get'] = array(
    'title' => 'Get or Update Queues',
    'description' => 'Load all queues from the SIF server.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sif_queues_get'),
    'access arguments' => array('configure sif'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'pages.inc',
  );
  $items['admin/sif/queues/%/schedule'] = array(
    'title' => 'Schedule Queue Processing',
    'description' => 'Schedule how often this queue will be queried.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sif_queues_schedule', 3),
    'access arguments' => array('configure sif'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'pages.inc',
  );


  // SIF Object View page.
  $items['sif/%sif_object/messages'] = array(
    'title' => 'Messages',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sif_queues_get_messages', 1),
    'access callback' => 'sif_queues_menu_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'messages.inc',
  );

  // Subscriptions
  $items['admin/sif/queues/%/subscriptions'] = array(
    'title' => 'Subscriptions',
    'description' => 'Create and manage SIF subscriptions.',
    'page callback' => 'sif_subscriptions_page',
    'page arguments' => array(3),
    'access arguments' => array('configure sif'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'subscriptions.inc',
  );
  $items['admin/sif/queues/%/subscriptions/create'] = array(
    'title' => 'Create Subscription',
    'description' => 'Create a new subscription.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sif_subscriptions_create', 3),
    'access arguments' => array('configure sif'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'subscriptions.inc',
  );
  $items['admin/sif/queues/%/subscriptions/get'] = array(
    'title' => 'Get or Update Subscriptions',
    'description' => 'Load all subscriptions from the SIF server.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sif_subscriptions_get', 3),
    'access arguments' => array('configure sif'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'subscriptions.inc',
  );
  return $items;
}

/**
 *  Only show up on queues.
 */
function sif_queues_menu_access($sif_object){
  return $sif_object->type == 'queue';
}

/**
 * Implements hook_cron_queue_info()
 */
function sif_queues_cron_queue_info() {
  $queues = array();
  $queues['sif_queue_list'] = array(
    'worker callback' => 'sif_queues_queueprocess',
    'time' => 60,
  );
  return $queues;
}

/**
* Implements hook_cron().
*/
function sif_queues_cron() {
  $drupal_queue = DrupalQueue::get('sif_queue_list', TRUE);

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'sif_data_object')
    ->entityCondition('bundle', 'queue');
  $result = $query->execute();

  if (isset($result['sif_data_object'])) {
    $object_ids = array_keys($result['sif_data_object']);
    $queues = entity_load('sif_data_object', $object_ids);
    foreach ($queues as $queue){
      //watchdog('sif_queues', 'Queue: !queue', array('!queue' => (string) $queue->id));
      $timing = variable_get('sif_queue_' . $queue->id, array('length' => 1, 'period' => 'hour', 'status' => 'enable', 'last' => time(), 'drupalqueue' => 0));
      if (!$timing['last']) {
        $timing['last'] = time();
      }
      if (time() >= strtotime('plus ' . $timing['length'] . ' ' . $timing['period'], $timing['last'])) {
        $drupal_queue->createItem($queue->id);
      }
    }
  }
}

/**
 * Processes the SIF queues stored in the drupal queue
 */
function sif_queues_queueprocess($queue) {
  $queue = check_plain($queue);
  $last_message = variable_get("sif_queue_last_$queue", '');
  $settings = variable_get("sif_queue_$queue", array('length' => 1, 'period' => 'hour', 'status' => 'enable', 'last' => time(), 'drupalqueue' => 0));
  if ($settings['status'] == 'enable'){
    if ($settings['drupalqueue']) {
      $drupal_queue = DrupalQueue::get("sif_queue_items", TRUE);
    }
    while ($results = sif_get('queue', array('id' => $queue, 'path' => 'messages', 'multi' => TRUE, 'deleteMessageId' => $last_message))) {
      if ($xml_in = new SimpleXMLElement($results->data)) {
        $type = preg_replace("/^sif:/", '', $xml_in->getName());
        if (preg_match("/s$/", $type)) {
          $type = rtrim($type, 's');
          $xml = $xml_in->$type;
        }
        else {
          $xml = $xml_in;
        }
        if (property_exists($xml->attributes(), 'id')) {
          $id = check_plain(strval($xml->attributes()->id));
        }
        else {
          $id = check_plain(strval($xml->attributes()->refId));
        }
        sif_store($type, $xml->asXML(), $results->headers);
        if ($settings['drupalqueue']) {
          $drupal_queue->createItem($id);
        }
        variable_set("sif_queue_last_$queue", $id);
      }
    }
    variable_set("sif_queue_last_$queue", '');
  }
}
